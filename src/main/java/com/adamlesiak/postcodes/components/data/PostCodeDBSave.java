package com.adamlesiak.postcodes.components.data;

import com.adamlesiak.postcodes.persistence.transfer.PostCodeDTO;
import com.adamlesiak.postcodes.persistence.mapper.PostCodeDTOToDocumentMapper;
import com.adamlesiak.postcodes.persistence.document.PostCodeDocument;
import com.adamlesiak.postcodes.persistence.repository.PostCodeRepository;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class PostCodeDBSave {

    private PostCodeLoader postCodeLoader;
    private PostCodeRepository postCodeRepository;
    private PostCodeDTOToDocumentMapper mapper;

    public PostCodeDBSave(PostCodeLoader postCodeLoader, PostCodeRepository postCodeRepository, PostCodeDTOToDocumentMapper mapper) {
        this.postCodeLoader = postCodeLoader;
        this.postCodeRepository = postCodeRepository;
        this.mapper = mapper;
    }

    @PostConstruct
    private void save() {
        Set<PostCodeDTO> postCodeDTOSet = postCodeLoader.getPostCodeDTOSet();
        Set<PostCodeDocument> postCodeDocuments = postCodeDTOSet.stream().map(mapper).collect(Collectors.toSet());
        postCodeRepository.saveAll(postCodeDocuments);
    }

}
