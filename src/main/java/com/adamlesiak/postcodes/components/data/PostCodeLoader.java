package com.adamlesiak.postcodes.components.data;

import com.adamlesiak.postcodes.persistence.transfer.PostCodeDTO;
import lombok.Getter;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

@Getter
@Component
@Lazy
public class PostCodeLoader {

    private Set<PostCodeDTO> postCodeDTOSet = new HashSet<>();

    @PostConstruct
    private void parsePostCodes() {
        String fileData = loadFIleData();
        String[] fileDataArray = fileData.split("\n");
        for (String fileDataRow : fileDataArray) {
            String[] fileDataRowArray = fileDataRow.split(";");
            postCodeDTOSet
                    .add(
                            PostCodeDTO.builder()
                                    .postCode(fileDataRowArray[0])
                                    .street(fileDataRowArray[1])
                                    .city(fileDataRowArray[2])
                                    .region(fileDataRowArray[3])
                                    .district(fileDataRowArray[4])
                                    .build()
                    );
        }

        System.out.println(postCodeDTOSet);
    }

    private String loadFIleData() {
        InputStream inputStream = getClass().getResourceAsStream("/data/postcodes.txt");
        Scanner scanner = new Scanner(inputStream).useDelimiter("\\A");
        return scanner.hasNext() ? scanner.next() : "";
    }


}
