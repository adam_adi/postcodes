package com.adamlesiak.postcodes.domain.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AngularJSTemplateController {

    @RequestMapping(value = "/angular-templates/{name}")
    public String projectsList(@PathVariable(value = "name") String name) {
        return "/angular-templates/" + name;
    }
}
