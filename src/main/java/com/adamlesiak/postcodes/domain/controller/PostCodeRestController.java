package com.adamlesiak.postcodes.domain.controller;

import com.adamlesiak.postcodes.domain.enums.DocumentFilterRequestHeader;
import com.adamlesiak.postcodes.persistence.document.PostCodeDocument;
import com.adamlesiak.postcodes.persistence.loader.PostCodeDataLoader;
import lombok.Builder;
import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/api/v1")
public class PostCodeRestController {

    private PostCodeDataLoader postCodeDataLoader;

    public PostCodeRestController(PostCodeDataLoader postCodeDataLoader) {
        this.postCodeDataLoader = postCodeDataLoader;
    }

    @GetMapping(value = "/post-codes")
    public ResponseEntity<List<PostCodeDocument>> loadAllDocuments() {
        return ResponseEntity.ok(postCodeDataLoader.loadDocuments("*", DocumentFilterRequestHeader.FILTERING_DATABASE));
    }

    @GetMapping(value = "/post-codes/{post-code-phrase}")
    public ResponseEntity<PostCodeResponse> loadDatabaseFilteredDocuments(
            @PathVariable(value = "post-code-phrase") String postCodePhrase,
            @RequestHeader(value = "filter-type") DocumentFilterRequestHeader filterType
    ) {

        Date startTime = new Date();
        List<PostCodeDocument> documents = postCodeDataLoader.loadDocuments(postCodePhrase, filterType);
        Date endTime = new Date();

        return ResponseEntity.ok(PostCodeResponse.builder()
                .responseTime((int) (endTime.getTime() - startTime.getTime()))
                .documents(documents)
                .build());
    }


    @Data
    @Builder
    private static class PostCodeResponse {
        private int responseTime;
        private List<PostCodeDocument> documents;
    }


}
