package com.adamlesiak.postcodes.domain.enums;

public enum DocumentFilterRequestHeader {
    FILTERING_DATABASE, FILTERING_STREAM, FILTERING_PARALLEL_STREAM
}
