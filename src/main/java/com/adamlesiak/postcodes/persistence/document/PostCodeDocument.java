package com.adamlesiak.postcodes.persistence.document;


import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.builder.HashCodeExclude;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

@Data
@Builder
@Document
@EqualsAndHashCode(of = "id")
public class PostCodeDocument {

    @Id
    @Builder.Default
    private String id = UUID.randomUUID().toString();

    private String postCode;
    private String street;
    private String city;
    private String region;
    private String district;
}
