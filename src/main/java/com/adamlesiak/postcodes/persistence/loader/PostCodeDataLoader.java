package com.adamlesiak.postcodes.persistence.loader;

import com.adamlesiak.postcodes.domain.enums.DocumentFilterRequestHeader;
import com.adamlesiak.postcodes.persistence.document.PostCodeDocument;
import com.adamlesiak.postcodes.persistence.repository.PostCodeRepository;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class PostCodeDataLoader {

    private PostCodeRepository postCodeRepository;

    public PostCodeDataLoader(PostCodeRepository postCodeRepository) {
        this.postCodeRepository = postCodeRepository;
    }

    public List<PostCodeDocument> loadDocuments(String postCodePhrase, DocumentFilterRequestHeader requestHeader) {
        switch (requestHeader) {
            case FILTERING_STREAM: {
                return postCodeRepository.
                        findAll()
                        .stream()
                        .filter(e -> e.getPostCode().startsWith(postCodePhrase))
                        .sorted(Comparator.comparing(PostCodeDocument::getStreet))
                        .collect(Collectors.toList());
            }
            case FILTERING_PARALLEL_STREAM: {
                return postCodeRepository.
                        findAll()
                        .parallelStream()
                        .filter(e -> e.getPostCode().startsWith(postCodePhrase))
                        .sorted(Comparator.comparing(PostCodeDocument::getStreet))
                        .collect(Collectors.toList());
            }
            case FILTERING_DATABASE:
            default: {
                return postCodeRepository.findAllByPostCodeStartsWithOrderByCityAscRegionAscStreetAsc(postCodePhrase);
            }
        }
    }

}
