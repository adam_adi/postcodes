package com.adamlesiak.postcodes.persistence.mapper;

import com.adamlesiak.postcodes.persistence.document.PostCodeDocument;
import com.adamlesiak.postcodes.persistence.transfer.PostCodeDTO;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class PostCodeDTOToDocumentMapper implements Function<PostCodeDTO, PostCodeDocument> {
    @Override
    public PostCodeDocument apply(PostCodeDTO postCodeDTO) {
        return PostCodeDocument.builder()
                .city(postCodeDTO.getCity())
                .district(postCodeDTO.getDistrict())
                .postCode(postCodeDTO.getPostCode())
                .region(postCodeDTO.getRegion())
                .street(postCodeDTO.getStreet())
                .build();
    }
}
