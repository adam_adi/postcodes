package com.adamlesiak.postcodes.persistence.repository;

import com.adamlesiak.postcodes.persistence.document.PostCodeDocument;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PostCodeRepository extends MongoRepository<PostCodeDocument, String> {

    List<PostCodeDocument> findAllByPostCodeStartsWithOrderByCityAscRegionAscStreetAsc(String postCode);

}
