package com.adamlesiak.postcodes.persistence.transfer;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PostCodeDTO {
    private String postCode;
    private String street;
    private String city;
    private String region;
    private String district;
}
