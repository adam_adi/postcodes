'use strict';

var App = angular.module("myApp", ["ngRoute"]);

App.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push(
        function() {
            return {
                request: function(config) {
                    if (config.url.indexOf("api") !== -1 && config.url.indexOf("noloader") === -1) {
                        CS_POPUP.openAjaXLoadingFull();
                    }
                    return config;
                },
                response: function(response) {
                    if (response.config.url.indexOf("api") !== -1) {
                        CS_POPUP.closeAjaXLoadingFull();
                    }
                    return response;
                }
            }}

        );

    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }
    //disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    // extra
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';

}]);

