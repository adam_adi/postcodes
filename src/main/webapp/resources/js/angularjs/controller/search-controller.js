'use strict';

App.controller('SearchFormController', ['$scope', '$http', '$q', function($scope, $http, $q) {
    $scope.test = function() {
        $http.get('/api/v1/post-codes/' + $scope.searchPhrase)
            .then(
                function (response) {
                    if (response.status === 200) {
                        console.log(response.data);
                    } else {
                        console.error('Error: Server returns other than 200(OK) status.');
                    }
                },
                function (errResponse) {
                    console.error('Error while fetching Items');
                    return $q.reject(errResponse);
                }
            );

    };
}]);