'use strict';

App.factory('SearchService', ['$http', '$q', function($http, $q) {
    return {
        searchPostCodes: function (postCodePart) {
            return $http.get('/post-codes/' + postCodePart)
                .then(
                    function (response) {
                        if (response.status === 204) {
                            return {response: 'no-data'};
                        } else {
                            return response.data;
                        }
                    },
                    function (errResponse) {
                        console.error('Error while fetching Items');
                        return $q.reject(errResponse);
                    }
                );
        }
    }
}]);